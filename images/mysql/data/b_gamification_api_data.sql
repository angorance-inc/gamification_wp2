-- -----------------------------------------------------
-- Import data for Application table
-- -----------------------------------------------------
USE `gamification_api`;

-- -----------------------------------------------------
-- Table Application
-- -----------------------------------------------------
INSERT INTO `gamification_api`.`application_entity` (`application_key`) VALUES
('API_KEY_1'),
('API_KEY_2'),
('API_KEY_3'),
('API_KEY_4'),
('API_KEY_5'),
('API_KEY_6'),
('API_KEY_7'),
('API_KEY_8'),
('API_KEY_9');