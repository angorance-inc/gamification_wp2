Feature: Handler of events

  Background:
    Given there is an Events server

  Scenario: Create an event whose triggered rule has no property and gives a badge
    Given I have an application key for my event
    And I have a badge
    And I have a rule with type type1, 1 badge and 0 pointScale with 0 points for this application
    And I have an event type1 for the user 1
    Then I receive a 200 status code for events operation
    And the user received the correct badge

  Scenario: Create an event whose triggered rule has no property and gives points to a pointScale
    Given I have an application key for my event
    And I have a pointScale
    And I have a rule with type type2, 0 badge and 1 pointScale with 50 points for this application
    And I have an event property named property0 with the value 2
    And I have an event type2 for the user 2
    Then I receive a 200 status code for events operation
    And the user received 50 points for this pointScale

  Scenario: Create an event whose triggered rule has no property and gives a badge and points to a pointScale
    Given I have an application key for my event
    And I have a badge
    And I have a pointScale
    And I have a rule with type type3, 1 badge and 1 pointScale with 120 points for this application
    And I have an event type3 for the user 3
    Then I receive a 200 status code for events operation
    And the user received the correct badge
    And the user received 120 points for this pointScale

  Scenario: Create an event whose triggered rule gives a badge
    Given I have an application key for my event
    And I have a badge
    And I have a property named property1 whose condition is to be higher than 2
    And I have a rule with type type4, 1 badge and 0 pointScale with 0 points for this application
    And I have an event property named property1 with the value 4
    And I have an event type4 for the user 4
    Then I receive a 200 status code for events operation
    And the user received the correct badge

  Scenario: Create an event whose triggered rule gives points to a pointScale
    Given I have an application key for my event
    And I have a pointScale
    And I have a property named property2 whose condition is to be lte than 5
    And I have a rule with type type5, 0 badge and 1 pointScale with 300 points for this application
    And I have an event property named property2 with the value 5
    And I have an event type5 for the user 5
    Then I receive a 200 status code for events operation
    And the user received 300 points for this pointScale

  Scenario: Create an event whose triggered rule gives a badge and points to a pointScale
    Given I have an application key for my event
    And I have a badge
    And I have a pointScale
    And I have a property named property3 whose condition is to be equal than 4
    And I have a rule with type type6, 1 badge and 1 pointScale with 20 points for this application
    And I have an event property named property3 with the value 4
    And I have an event type6 for the user 6
    Then I receive a 200 status code for events operation
    And the user received the correct badge
    And the user received 20 points for this pointScale

  Scenario: Create an event whose both triggered rules have no property and give points to the same pointScale
    Given I have an application key for my event
    And I have a pointScale
    And I have a rule with type type7, 0 badge and 1 pointScale with 75 points for this application
    And I have a rule with type type7, 0 badge and 1 pointScale with 25 points for this application
    And I have an event type7 for the user 7
    Then I receive a 200 status code for events operation
    And the user received 100 points for this pointScale

  Scenario: Create an event which has no triggered rule (not the good type to be triggered)
    Given I have an application key for my event
    And I have a pointScale
    And I have a rule with type type8, 0 badge and 1 pointScale with 10 points for this application
    And I have an event type88 for the user 8
    Then I receive a 200 status code for events operation
    And the user received 0 points for this pointScale

  Scenario: Create an event with two different triggered rules
    Given I have an application key for my event
    And I have a badge
    And I have a pointScale
    And I have a rule with type type9, 1 badge and 0 pointScale with 0 points for this application
    And I have a rule with type type9, 0 badge and 1 pointScale with 250 points for this application
    And I have an event type9 for the user 9
    Then I receive a 200 status code for events operation
    And the user received the correct badge
    And the user received 250 points for this pointScale

  Scenario: Create multiple events with the same userId
    Given I have an application key for my event
    And I have a pointScale
    And I have a rule with type type10, 0 badge and 1 pointScale with 125 points for this application
    And I have an event type10 for the user 10
    And I have an event type10 for the user 10
    Then I receive a 200 status code for events operation
    And the user received 250 points for this pointScale