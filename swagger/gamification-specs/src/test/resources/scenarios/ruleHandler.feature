Feature: Handler of rules

  Background:
    Given there is a Rules server

  Scenario: Create a simple rule with no property and a badge
    Given I have an application key for my rule
    And I have a rule
    And this rule is activated on event whose type is AskAQuestion
    And this rule gives a badge
    When I POST it to the /rules endpoint
    Then I receive a 201 status code for rules operation

  Scenario: Create a simple rule with no property and a pointScale
    Given I have an application key for my rule
    And I have a rule
    And this rule is activated on event whose type is AskAQuestion
    And this rule adds 100 points to a point scale
    When I POST it to the /rules endpoint
    Then I receive a 201 status code for rules operation

  Scenario: Create a complete rule with property, a badge and a pointScale
    Given I have an application key for my rule
    And I have a rule
    And this rule is activated on event whose type is AskAQuestion
    And this rule is activated when the property nbrAskedQuestion is higher than 5
    And this rule gives a badge
    And this rule adds 200 points to a point scale
    When I POST it to the /rules endpoint
    Then I receive a 201 status code for rules operation

  Scenario: Update a rule
    Given I have an application key for my rule
    And I have a rule
    And this rule is activated on event whose type is AskAQuestion
    And this rule is activated when the property nbrAskedQuestion is higher than 10
    And this rule gives a badge
    And this rule adds 200 points to a point scale
    When I POST it to the /rules endpoint
    Then I receive a 201 status code for rules operation
    Given I have an updated rule type AddEvaluation
    And I have an updated rule property condition: name - nbrAddedEvaluation, condition - gte, value - 20
    And I have an updated number of points 500
    When I PATCH the existing rule of the application
    Then I receive a 202 status code for rules operation

  Scenario: List rules
    Given I have an application key for my rule
    And I have a rule
    And this rule is activated on event whose type is AskAQuestion
    And this rule gives a badge
    When I POST it to the /rules endpoint
    Then I receive a 201 status code for rules operation
    Given I have an application key for my rule
    And I have a rule
    And this rule is activated on event whose type is AskAQuestion
    And this rule adds 100 points to a point scale
    When I POST it to the /rules endpoint
    Then I receive a 201 status code for rules operation
    When I GET the rules of the application
    Then I receive a 200 status code for rules operation
    And the response is a list containing at least 2 rules

  Scenario: Delete a rule
    Given I have an application key for my rule
    And I have a rule
    And this rule is activated on event whose type is AskAQuestion
    And this rule gives a badge
    When I POST it to the /rules endpoint
    Then I receive a 201 status code for rules operation
    When I DELETE my rule with the key
    Then I receive a 200 status code for rules operation