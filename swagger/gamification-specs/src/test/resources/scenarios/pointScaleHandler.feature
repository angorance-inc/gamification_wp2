Feature: Handler of pointScales

  Background:
    Given there is a PointScales server

  Scenario: Create a pointScale
    Given I have an application key for my pointScale
    And I have a pointScale payload
    When I POST it to the /pointScales endpoint
    Then I receive a 201 status code for pointScales operation

  Scenario: Update a pointScale
    Given I have an application key for my pointScale
    And I have a pointScale payload
    When I POST it to the /pointScales endpoint
    Then I receive a 201 status code for pointScales operation
    Given I have an updated pointScale payload
    When I PATCH the existing pointScale of the application
    Then I receive a 202 status code for pointScales operation

  Scenario: List pointScales
    Given I have an application key for my pointScale
    And I have a pointScale payload
    When I POST it to the /pointScales endpoint
    Then I receive a 201 status code for pointScales operation
    Given I have an application key for my pointScale
    And I have a pointScale payload
    When I POST it to the /pointScales endpoint
    Then I receive a 201 status code for pointScales operation
    When I GET the pointScales of the application
    Then I receive a 200 status code for pointScales operation
    And the response is a list containing at least 2 pointScales

  Scenario: Delete a pointScale
    Given I have an application key for my pointScale
    And I have a pointScale payload
    When I POST it to the /pointScales endpoint
    Then I receive a 201 status code for pointScales operation
    When I DELETE my pointScale with the key
    Then I receive a 200 status code for pointScales operation