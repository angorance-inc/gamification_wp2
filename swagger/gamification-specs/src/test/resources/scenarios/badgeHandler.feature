Feature: Handler of badges

  Background:
    Given there is a Badges server

  Scenario: Create a badge
    Given I have an application key for my badge
    And I have a badge payload
    When I POST it to the /badges endpoint
    Then I receive a 201 status code for badges operation

  Scenario: Update a badge
    Given I have an application key for my badge
    And I have a badge payload
    When I POST it to the /badges endpoint
    Then I receive a 201 status code for badges operation
    Given I have an updated badge payload
    When I PATCH the existing badge of the application
    Then I receive a 202 status code for badges operation

  Scenario: List badges
    Given I have an application key for my badge
    And I have a badge payload
    When I POST it to the /badges endpoint
    Then I receive a 201 status code for badges operation
    Given I have an application key for my badge
    And I have a badge payload
    When I POST it to the /badges endpoint
    Then I receive a 201 status code for badges operation
    When I GET the badges of the application
    Then I receive a 200 status code for badges operation
    And the response is a list containing at least 2 badges

  Scenario: Delete a badge
    Given I have an application key for my badge
    And I have a badge payload
    When I POST it to the /badges endpoint
    Then I receive a 201 status code for badges operation
    When I DELETE my badge with the key
    Then I receive a 200 status code for badges operation