package ch.heigvd.gamification.api.spec.steps;

import ch.heigvd.gamification.*;
import ch.heigvd.gamification.api.*;
import ch.heigvd.gamification.api.dto.*;
import ch.heigvd.gamification.api.spec.helpers.Environment;
import cucumber.api.java.en.*;

import java.sql.Timestamp;
import java.util.*;
import java.util.regex.*;

import static org.junit.Assert.*;

public class RulesSteps {

    private Environment environment;
    private RulesApi api;
    private BadgesApi apiBadges;
    private PointScalesApi apiPointScales;

    String applicationKey;
    List<Rule> rules;

    Long ruleId;
    Rule rule;
    RuleIf ruleIf;
    RuleIfProperty ruleIfProperty;

    RuleThen ruleThen;
    RuleThenAwardPoints ruleThenAwardPoints;

    Badge badge;
    Long badgeId;
    PointScale pointScale;
    Long pointScaleId;

    private ApiResponse lastApiResponse;
    private ApiException lastApiException;
    private boolean lastApiCallThrewException;
    private int lastStatusCode;

    public RulesSteps(Environment environment) {
        this.environment = environment;
        this.api = environment.getRulesApi();
        this.apiBadges = environment.getBadgesApi();
        this.apiPointScales = environment.getPointScalesApi();
    }

    /*-----------------------------------------------------------------
    /* Creation feature ---------------------------------------------*/

    @Given("^there is a Rules server$")
    public void there_is_a_Rules_server() throws Throwable {
        assertNotNull(api);
    }

    @Given("^I have an application key for my rule")
    public void i_have_an_application_key_for_rules() throws Throwable {
        applicationKey = "API_KEY_3";
    }

    @Given("^I have a rule$")
    public void i_have_a_rule() throws Throwable {

        // Initialization
        rule = new Rule();
        ruleIf = new RuleIf();
        ruleThen = new RuleThen();
        ruleIfProperty = new RuleIfProperty();
        ruleThenAwardPoints = new RuleThenAwardPoints();

        ruleIf.setType(null);
        ruleIfProperty.setName(null);
        ruleIfProperty.setCondition(null);
        ruleIfProperty.setValue(null);

        ruleThen.setAwardBadge(null);
        ruleThenAwardPoints.setPoints(null);
        ruleThenAwardPoints.setPointScale(null);

        // Construction
        ruleIf.setProperty(ruleIfProperty);
        ruleThen.setAwardPoints(ruleThenAwardPoints);

        rule.setIf(ruleIf);
        rule.setThen(ruleThen);
    }

    @Given("^this rule is activated on event whose type is (\\w+)$")
    public void this_rule_triggers_an_event_type(String type) throws Throwable {
        ruleIf.setType(type);
    }

    @Given("^this rule is activated when the property (\\w+) is (\\w+) than (\\w+)$")
    public void this_rule_triggers_only_when_a_condition_is_fullfield(String property, String condition, String value) throws Throwable {

        // Set the condition
        ruleIfProperty.setName(property);
        ruleIfProperty.setCondition(condition);
        ruleIfProperty.setValue(value);
    }

    @Given("^this rule gives a badge$")
    public void this_rule_gives_a_badge() throws Throwable {

        // Create the badge
        badge = new Badge();
        badge.setName("BADGE_" + new Timestamp(System.currentTimeMillis()));

        // Post the badge
        try {
            lastApiResponse = apiBadges.createBadgeWithHttpInfo(applicationKey, badge);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();

            // Get id of the created badge from the location header
            Map<String, List<String>> headers = lastApiResponse.getHeaders();
            List<String> locationList = headers.get("Location");

            if(locationList != null) {
                String location = locationList.get(0);
                assertNotNull(location);

                Pattern p = Pattern.compile(".*/badges/(.+)$");
                Matcher m = p.matcher(location);
                m.find();

                badgeId = Long.parseLong(m.group(1));
            }

        } catch (ApiException e) {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }

        // Add the badge to the rule
        ruleThen.setAwardBadge(badgeId);
    }

    @Given("^this rule adds (\\d+) points to a point scale$")
    public void this_rule_add_points_to_point_scale(long points) throws Throwable {

        // Create the pointScale
        pointScale = new PointScale();
        pointScale.setName("POINTSCALE_" + new Timestamp(System.currentTimeMillis()));

        // Post the pointScale
        try {
            lastApiResponse = apiPointScales.createPointScaleWithHttpInfo(applicationKey, pointScale);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();

            // Get id of the created PointScale from the location header
            Map<String, List<String>> headers = lastApiResponse.getHeaders();
            List<String> locationList = headers.get("Location");

            if(locationList != null) {
                String location = locationList.get(0);
                assertNotNull(location);

                Pattern p = Pattern.compile(".*/pointScales/(.+)$");
                Matcher m = p.matcher(location);
                m.find();

                pointScaleId = Long.parseLong(m.group(1));
            }

        } catch (ApiException e) {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }

        // Add the points and the pointscale to the rule
        ruleThenAwardPoints.setPoints(points);
        ruleThenAwardPoints.setPointScale(pointScaleId);
    }

    @When("^I POST it to the /rules endpoint$")
    public void i_POST_it_to_the_rules_endpoint() throws Throwable {
        try {
            lastApiResponse = api.createRuleWithHttpInfo(applicationKey, rule);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();

            // Get id of the created rule from the location header
            Map<String, List<String>> headers = lastApiResponse.getHeaders();
            List<String> locationList = headers.get("Location");

            if(locationList != null) {
                String location = locationList.get(0);
                assertNotNull(location);

                Pattern p = Pattern.compile(".*/rules/(.+)$");
                Matcher m = p.matcher(location);
                m.find();

                ruleId = Long.parseLong(m.group(1));

            }

        } catch (ApiException e) {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @Then("^I receive a (\\d+) status code for rules operation$")
    public void i_receive_a_status_code_for_rules_operation(int arg1) throws Throwable {
        assertEquals(arg1, lastStatusCode);
    }

    /*-----------------------------------------------------------------
    /* Retrieval feature --------------------------------------------*/

    @When("^I GET the rules of the application$")
    public void i_GET_the_rules_by_application_key() throws Throwable {
        try
        {
            lastApiResponse = api.getRulesWithHttpInfo(applicationKey);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @And("^the response is a list containing at least (\\d+) rules$")
    public void theResponseIsAListContainingRules(int nb) throws Throwable
    {
        rules = (List<Rule>)lastApiResponse.getData();

        assertNotNull(rules);
        assert(nb <= rules.size());
    }

    /*-----------------------------------------------------------------
    /* Update feature -----------------------------------------------*/

    @Given("^I have an updated rule type (\\w+)$")
    public void i_have_an_updated_rule_type(String type) throws Throwable{
        ruleIf.setType(type);
    }

    @Given("^I have an updated rule property condition: name - (\\w+), condition - (\\w+), value - (\\w+)$")
    public void i_have_an_updated_rule_property(String name, String condition, String value) throws Throwable{
        ruleIfProperty.setName(name);
        ruleIfProperty.setCondition(condition);
        ruleIfProperty.setValue(value);
    }

    @Given("^I have an updated number of points (\\d+)$")
    public void i_have_an_updated_rule_points(long points) throws Throwable{
        ruleThenAwardPoints.setPoints(points);
    }

    @When("^I PATCH the existing rule of the application$")
    public void i_PATCH_the_existing_rule_of_the_application() throws Throwable {
        try
        {
            lastApiResponse = api.updateRuleWithHttpInfo(applicationKey, ruleId, rule);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    /*-----------------------------------------------------------------
    /* Deletion feature ---------------------------------------------*/

    @When("^I DELETE my rule with the key$")
    public void i_DELETE_the_rule() throws Throwable {
        try
        {
            lastApiResponse = api.deleteRuleWithHttpInfo(applicationKey, ruleId);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }
}
