package ch.heigvd.gamification.api.spec.steps;

import ch.heigvd.gamification.*;
import ch.heigvd.gamification.api.*;
import ch.heigvd.gamification.api.dto.*;
import ch.heigvd.gamification.api.spec.helpers.Environment;
import cucumber.api.java.en.*;

import java.sql.Timestamp;
import java.util.*;
import java.util.regex.*;

import static org.junit.Assert.*;

public class EventsSteps {

    private Environment environment;
    private EventsApi api;
    private BadgesApi apiBadges;
    private PointScalesApi apiPointScales;
    private RulesApi apiRules;
    private EventsApi apiEvents;
    private UsersApi apiUsers;

    String applicationKey;

    Long userId;
    Map<String, String> properties = new HashMap<String, String>();

    Badge badge;
    Long badgeId;
    PointScale pointScale;
    Long pointScaleId;

    Long ruleId;
    Rule rule;
    RuleIf ruleIf;
    RuleIfProperty ruleIfProperty;
    RuleThen ruleThen;
    RuleThenAwardPoints ruleThenAwardPoints;

    String property = null;
    String condition = null;
    String value = null;

    List<UserBadge> userBadges = new ArrayList<>();
    List<UserPointScale> userPointScales = new ArrayList<>();

    private ApiResponse lastApiResponse;
    private ApiException lastApiException;
    private boolean lastApiCallThrewException;
    private int lastStatusCode;

    public EventsSteps(Environment environment) {
        this.environment = environment;
        this.api = environment.getEventsApi();
        this.apiBadges = environment.getBadgesApi();
        this.apiPointScales = environment.getPointScalesApi();
        this.apiRules = environment.getRulesApi();
        this.apiEvents = environment.getEventsApi();
        this.apiUsers = environment.getUsersApi();
    }

    @Given("^there is an Events server$")
    public void there_is_an_Events_server() throws Throwable {
        assertNotNull(api);
    }

    @Given("^I have an application key for my event")
    public void i_have_an_application_key_for_events() throws Throwable {
        applicationKey = "API_KEY_4";
    }

    @Given("^I have a badge$")
    public void i_have_a_badge() throws Throwable {

        // Create the badge
        badge = new Badge();
        badge.setName("BADGE_" + new Timestamp(System.currentTimeMillis()));

        // Post the badge
        try {
            lastApiResponse = apiBadges.createBadgeWithHttpInfo(applicationKey, badge);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();

            // Get id of the created badge from the location header
            Map<String, List<String>> headers = lastApiResponse.getHeaders();
            List<String> locationList = headers.get("Location");

            if(locationList != null) {
                String location = locationList.get(0);
                assertNotNull(location);

                Pattern p = Pattern.compile(".*/badges/(.+)$");
                Matcher m = p.matcher(location);
                m.find();

                badgeId = Long.parseLong(m.group(1));
            }

        } catch (ApiException e) {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @Given("^I have a pointScale$")
    public void i_have_a_pointscale() throws Throwable {

        // Create the point scale
        pointScale = new PointScale();
        pointScale.setName("POINTSCALE_" + new Timestamp(System.currentTimeMillis()));

        // Post the point scale
        try {
            lastApiResponse = apiPointScales.createPointScaleWithHttpInfo(applicationKey, pointScale);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();

            // Get id of the created badge from the location header
            Map<String, List<String>> headers = lastApiResponse.getHeaders();
            List<String> locationList = headers.get("Location");

            if(locationList != null) {
                String location = locationList.get(0);
                assertNotNull(location);

                Pattern p = Pattern.compile(".*/pointScales/(.+)$");
                Matcher m = p.matcher(location);
                m.find();

                pointScaleId = Long.parseLong(m.group(1));
            }

        } catch (ApiException e) {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @Given("^I have a property named (\\w+) whose condition is to be (\\w+) than (\\w+)$")
    public void i_have_a_property(String property, String condition, String value) throws Throwable {
        this.property = property;
        this.condition = condition;
        this.value = value;
    }

    @Given("^I have a rule with type (\\w+), (\\d+) badge and (\\d+) pointScale with (\\d+) points for this application$")
    public void i_have_a_rule(String type, long nbrBadge, long nbrPointScale, long nbrPoints) throws Throwable {

        // Create the rule
        rule = new Rule();
        ruleIf = new RuleIf();
        ruleThen = new RuleThen();
        ruleIfProperty = new RuleIfProperty();
        ruleThenAwardPoints = new RuleThenAwardPoints();

        // Add the type
        ruleIf.setType(type);

        // Add the property
        ruleIfProperty.setName(property);
        ruleIfProperty.setCondition(condition);
        ruleIfProperty.setValue(value);

        // Add the badge and pointScale
        ruleThen.setAwardBadge(nbrBadge == 1 ? badgeId : null);
        ruleThenAwardPoints.setPointScale(nbrPointScale == 1 ? pointScaleId : null);
        ruleThenAwardPoints.setPoints(nbrPointScale == 1 ? nbrPoints : null);

        // Construction
        ruleIf.setProperty(ruleIfProperty);
        ruleThen.setAwardPoints(ruleThenAwardPoints);
        rule.setIf(ruleIf);
        rule.setThen(ruleThen);

        // Post the rule
        try {
            lastApiResponse = apiRules.createRuleWithHttpInfo(applicationKey, rule);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();

            // Get id of the created badge from the location header
            Map<String, List<String>> headers = lastApiResponse.getHeaders();
            List<String> locationList = headers.get("Location");

            if(locationList != null) {
                String location = locationList.get(0);
                assertNotNull(location);

                Pattern p = Pattern.compile(".*/rules/(.+)$");
                Matcher m = p.matcher(location);
                m.find();

                badgeId = Long.parseLong(m.group(1));
            }

        } catch (ApiException e) {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @Given("^I have an event property named (\\w+) with the value (\\w+)$")
    public void i_have_an_event_property(String property, String value) throws Throwable {
        properties.put(property, value);
    }

    @Given("^I have an event (\\w+) for the user (\\w+)$")
    public void i_have_an_event(String type, long userId) {

        // Set the userId
        this.userId = userId;

        // Create the event
        Event event = new Event();
        event.setUserId(this.userId);
        event.setType(type);
        event.setProperties(properties);
        event.setDate(new Timestamp(System.currentTimeMillis()).toString());

        // Post the event
        try {
            lastApiResponse = apiEvents.createEventWithHttpInfo(applicationKey, event);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();

            // Get id of the created badge from the location header
            Map<String, List<String>> headers = lastApiResponse.getHeaders();
            List<String> locationList = headers.get("Location");

            if(locationList != null) {
                String location = locationList.get(0);
                assertNotNull(location);

                Pattern p = Pattern.compile(".*/events/(.+)$");
                Matcher m = p.matcher(location);
                m.find();

                badgeId = Long.parseLong(m.group(1));
            }

        } catch (ApiException e) {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @Then("^I receive a (\\d+) status code for events operation$")
    public void i_receive_a_status_code_for_events_operation(int arg1) throws Throwable {
        assertEquals(arg1, lastStatusCode);
    }

    @Then("^the user received the correct badge$")
    public void the_badge_was_given() {

        // Get all user's badges for the application
        try
        {
            lastApiResponse = apiUsers.getUserBadgesWithHttpInfo(applicationKey, userId);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }

        userBadges = (List<UserBadge>)lastApiResponse.getData();

        // Check if the badge was given to the user
        assertTrue(userBadges.size() != 0 && userBadges.get(0).getName().compareTo(badge.getName()) == 0);
    }

    @Then("^the user received (\\d+) points for this pointScale$")
    public void the_points_were_given(long points) {

        // Get all user's pointScales for the application
        try
        {
            lastApiResponse = apiUsers.getUserPointScalesWithHttpInfo(applicationKey, userId);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }

        userPointScales = (List<UserPointScale>)lastApiResponse.getData();

        // Check if the badge was given to the user
        if(points != 0) {
            assertTrue (userPointScales.size() != 0
                    && userPointScales.get(0).getName().compareTo(pointScale.getName()) == 0
                    && userPointScales.get(0).getPoints() == points);

        } else {
            assertTrue(userPointScales.size() == 0);
        }
    }
}
