package ch.heigvd.gamification.api.spec.steps;

import ch.heigvd.gamification.ApiException;
import ch.heigvd.gamification.ApiResponse;
import ch.heigvd.gamification.api.BadgesApi;
import ch.heigvd.gamification.api.dto.Badge;
import ch.heigvd.gamification.api.spec.helpers.Environment;
import cucumber.api.java.en.*;

import java.sql.Timestamp;
import java.util.*;
import java.util.regex.*;

import static org.junit.Assert.*;

public class BadgesSteps {

    private Environment environment;
    private BadgesApi api;

    String applicationKey;
    Long badgeId;
    Badge badge;
    List<Badge> badges;

    private ApiResponse lastApiResponse;
    private ApiException lastApiException;
    private boolean lastApiCallThrewException;
    private int lastStatusCode;

    public BadgesSteps(Environment environment) {
        this.environment = environment;
        this.api = environment.getBadgesApi();
    }

    /*-----------------------------------------------------------------
    /* Creation feature ---------------------------------------------*/

    @Given("^there is a Badges server$")
    public void there_is_a_Badges_server() throws Throwable {
        assertNotNull(api);
    }

    @Given("^I have an application key for my badge$")
    public void i_have_an_application_key_for_badges() throws Throwable {
        applicationKey = "API_KEY_1";
    }

    @Given("^I have a badge payload$")
    public void i_have_a_badge_payload() throws Throwable {
        badge = new Badge();
        badge.setName("BADGE_" + new Timestamp(System.currentTimeMillis()));
    }

    @When("^I POST it to the /badges endpoint$")
    public void i_POST_it_to_the_badges_endpoint() throws Throwable {
        try {
            lastApiResponse = api.createBadgeWithHttpInfo(applicationKey, badge);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();

            // Get id of the created badge from the location header
            Map<String, List<String>> headers = lastApiResponse.getHeaders();
            List<String> locationList = headers.get("Location");

            if(locationList != null) {
                String location = locationList.get(0);
                assertNotNull(location);

                Pattern p = Pattern.compile(".*/badges/(.+)$");
                Matcher m = p.matcher(location);
                m.find();

                badgeId = Long.parseLong(m.group(1));

            }

        } catch (ApiException e) {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @Then("^I receive a (\\d+) status code for badges operation$")
    public void i_receive_a_status_code_for_badges_operation(int arg1) throws Throwable {
        assertEquals(arg1, lastStatusCode);
    }

    /*-----------------------------------------------------------------
    /* Retrieval feature --------------------------------------------*/

    @When("^I GET the badges of the application$")
    public void i_GET_the_badges_by_application_key() throws Throwable {
        try
        {
            lastApiResponse = api.getBadgesWithHttpInfo(applicationKey);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @And("^the response is a list containing at least (\\d+) badges$")
    public void theResponseIsAListContainingBadges(int nb) throws Throwable
    {
        badges = (List<Badge>)lastApiResponse.getData();

        assertNotNull(badges);
        assert(nb <= badges.size());
    }

    /*-----------------------------------------------------------------
    /* Update feature -----------------------------------------------*/

    @Given("^I have an updated badge payload$")
    public void i_have_an_updated_badge_payload() throws Throwable{
        badge.setName("BADGE_" + new Timestamp(System.currentTimeMillis()));
    }

    @When("^I PATCH the existing badge of the application$")
    public void i_PATCH_the_existing_badge_of_the_application() throws Throwable {
        try
        {
            lastApiResponse = api.updateBadgeWithHttpInfo(applicationKey, badgeId, badge);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    /*-----------------------------------------------------------------
    /* Deletion feature ---------------------------------------------*/

    @When("^I DELETE my badge with the key$")
    public void i_DELETE_the_badge() throws Throwable {
        try
        {
            lastApiResponse = api.deleteBadgeWithHttpInfo(applicationKey, badgeId);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }
}
