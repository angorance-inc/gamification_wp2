package ch.heigvd.gamification.api.spec.helpers;

import ch.heigvd.gamification.api.*;

import java.io.IOException;
import java.util.Properties;

public class Environment {

    // API of gamification
    private BadgesApi badgesApi = new BadgesApi();
    private PointScalesApi pointScalesApi = new PointScalesApi();
    private RulesApi rulesApi = new RulesApi();
    private EventsApi eventsApi = new EventsApi();
    private UsersApi usersApi = new UsersApi();

    public Environment() throws IOException {
        Properties properties = new Properties();
        properties.load(this.getClass().getClassLoader().getResourceAsStream("environment.properties"));
        String url = properties.getProperty("ch.heigvd.gamification.server.url");

        // Initialize api
        badgesApi.getApiClient().setBasePath(url);
        pointScalesApi.getApiClient().setBasePath(url);
        eventsApi.getApiClient().setBasePath(url);
        rulesApi.getApiClient().setBasePath(url);
        usersApi.getApiClient().setBasePath(url);
    }

    public BadgesApi getBadgesApi() {
        return badgesApi;
    }

    public PointScalesApi getPointScalesApi() {
        return pointScalesApi;
    }

    public RulesApi getRulesApi() {
        return rulesApi;
    }

    public EventsApi getEventsApi() {
        return eventsApi;
    }

    public UsersApi getUsersApi() {
        return usersApi;
    }
}
