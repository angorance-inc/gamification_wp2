package ch.heigvd.gamification.api.spec.steps;

import ch.heigvd.gamification.ApiException;
import ch.heigvd.gamification.ApiResponse;
import ch.heigvd.gamification.api.PointScalesApi;
import ch.heigvd.gamification.api.dto.PointScale;
import ch.heigvd.gamification.api.spec.helpers.Environment;
import cucumber.api.java.en.*;

import java.sql.Timestamp;
import java.util.*;
import java.util.regex.*;

import static org.junit.Assert.*;

public class PointScalesSteps {

    private Environment environment;
    private PointScalesApi api;

    String applicationKey;
    Long pointScaleId;
    PointScale pointScale;
    List<PointScale> pointScales;

    private ApiResponse lastApiResponse;
    private ApiException lastApiException;
    private boolean lastApiCallThrewException;
    private int lastStatusCode;

    public PointScalesSteps(Environment environment) {
        this.environment = environment;
        this.api = environment.getPointScalesApi();
    }

    /*-----------------------------------------------------------------
    /* Creation feature ---------------------------------------------*/

    @Given("^there is a PointScales server$")
    public void there_is_a_PointScales_server() throws Throwable {
        assertNotNull(api);
    }

    @Given("^I have an application key for my pointScale$")
    public void i_have_an_application_key_for_pointscale() throws Throwable {
        applicationKey = "API_KEY_2";
    }

    @Given("^I have a pointScale payload$")
    public void i_have_a_PointScale_payload() throws Throwable {
        pointScale = new PointScale();
        pointScale.setName("POINTSCALE_" + new Timestamp(System.currentTimeMillis()));
    }

    @When("^I POST it to the /pointScales endpoint$")
    public void i_POST_it_to_the_pointScales_endpoint() throws Throwable {
        try {
            lastApiResponse = api.createPointScaleWithHttpInfo(applicationKey, pointScale);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();

            // Get id of the created PointScale from the location header
            Map<String, List<String>> headers = lastApiResponse.getHeaders();
            List<String> locationList = headers.get("Location");

            if(locationList != null) {
                String location = locationList.get(0);
                assertNotNull(location);

                Pattern p = Pattern.compile(".*/pointScales/(.+)$");
                Matcher m = p.matcher(location);
                m.find();

                pointScaleId = Long.parseLong(m.group(1));

            }

        } catch (ApiException e) {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @Then("^I receive a (\\d+) status code for pointScales operation$")
    public void i_receive_a_status_code_for_pointscales_operation(int arg1) throws Throwable {
        assertEquals(arg1, lastStatusCode);
    }

    /*-----------------------------------------------------------------
    /* Retrieval feature --------------------------------------------*/

    @When("^I GET the pointScales of the application$")
    public void i_GET_the_pointScales_by_application_key() throws Throwable {
        try
        {
            lastApiResponse = api.getPointScalesWithHttpInfo(applicationKey);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    @And("^the response is a list containing at least (\\d+) pointScales$")
    public void theResponseIsAListContainingPointScales(int nb) throws Throwable
    {
        pointScales = (List<PointScale>)lastApiResponse.getData();

        assertNotNull(pointScales);
        assert(nb <= pointScales.size());
    }

    /*-----------------------------------------------------------------
    /* Update feature -----------------------------------------------*/

    @Given("^I have an updated pointScale payload$")
    public void i_have_an_updated_PointScale_payload() throws Throwable{
        pointScale.setName("POINTSCALE_" + new Timestamp(System.currentTimeMillis()));
    }

    @When("^I PATCH the existing pointScale of the application$")
    public void i_PATCH_the_existing_PointScale_of_the_application() throws Throwable {
        try
        {
            lastApiResponse = api.updatePointScaleWithHttpInfo(applicationKey, pointScaleId, pointScale);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }

    /*-----------------------------------------------------------------
    /* Deletion feature ---------------------------------------------*/

    @When("^I DELETE my pointScale with the key$")
    public void i_DELETE_the_PointScale() throws Throwable {
        try
        {
            lastApiResponse = api.deletePointScaleWithHttpInfo(applicationKey, pointScaleId);
            lastApiCallThrewException = false;
            lastApiException = null;
            lastStatusCode = lastApiResponse.getStatusCode();
        }
        catch (ApiException e)
        {
            lastApiCallThrewException = true;
            lastApiResponse = null;
            lastApiException = e;
            lastStatusCode = lastApiException.getCode();
        }
    }
}
