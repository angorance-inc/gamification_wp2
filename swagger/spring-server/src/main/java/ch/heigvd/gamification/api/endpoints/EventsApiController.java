package ch.heigvd.gamification.api.endpoints;

import ch.heigvd.gamification.api.EventsApi;
import ch.heigvd.gamification.api.model.Event;
import ch.heigvd.gamification.entities.*;
import ch.heigvd.gamification.repositories.*;
import io.swagger.annotations.ApiParam;
import org.hibernate.LockMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.LockModeType;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-07-26T19:36:34.802Z")

/**
 * Controller for API Events.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Controller
public class EventsApiController implements EventsApi {

    @Autowired
    RuleRepository RuleRepository;

    @Autowired
    UserRepository UserRepository;

    @Autowired
    ApplicationRepository ApplicationRepository;

    @Autowired
    UserBadgesRepository UserBadgesRepository;

    @Autowired
    PointsAwardRepository PointsAwardRepository;

    @Autowired
    UserPointScalesRepository UserPointScalesRepository;

    @Transactional
    public ResponseEntity<Void> createEvent(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                            @ApiParam(value = "" ,required=true ) @RequestBody Event event) {

        // Get the user concerned by the event or create it if not existing
        UserEntity user = getOrCreateTheUser(event.getUserId());

        // Get the application concerned by the event
        ApplicationEntity application = ApplicationRepository.findByKey(authorization);

        // For each rule triggered, if the condition is fullfilled, give an award to the concerned user
        for (RuleEntity ruleFromDb : RuleRepository.findAllByApplicationAndType(application, event.getType())) {

            // If there is no condition or the condition is fullfilled
            if(ruleFromDb.getProperty() == null || conditionIsFullfilled(event, ruleFromDb.getProperty(), ruleFromDb.getCondition(), ruleFromDb.getValue())) {

                // Give a badge to the user
                if(ruleFromDb.getBadge() != null) {

                    // Create the badge award
                    UserBadgesEntity userBadge = new UserBadgesEntity();
                    userBadge.setUser(user);
                    userBadge.setBadge(ruleFromDb.getBadge());
                    userBadge.setTimestamp(Timestamp.valueOf(event.getDate()));

                    // Send the badge award
                    UserBadgesRepository.save(userBadge);
                }

                // Give some points to the user's point scale and update his total of points
                if(ruleFromDb.getPointScale() != null) {

                    // Create the points award
                    PointsAwardEntity pointsAward = new PointsAwardEntity();
                    pointsAward.setUser(user);
                    pointsAward.setPointScale(ruleFromDb.getPointScale());
                    pointsAward.setTimestamp(Timestamp.valueOf(event.getDate()));
                    pointsAward.setPoints(ruleFromDb.getPoints());

                    // Send the points award
                    PointsAwardRepository.save(pointsAward);


                    // Add the points to the user's point scale
                    updateUserPoints(user, ruleFromDb);
                }
            }
        }

        return ResponseEntity.ok().build();
    }

    private boolean conditionIsFullfilled(Event event, String property, String condition, String value) {

        boolean fullfilled = false;

        // Get the property of the event
        String eventValue = event.getProperties().get(property);

        // Check if the condition is fullfilled
        if(eventValue != null) {

            switch (condition) {
                case "higher":
                    fullfilled = Float.valueOf(eventValue) > Float.valueOf(value);
                    break;

                case "gte":
                    fullfilled = Float.valueOf(eventValue) >= Float.valueOf(value);
                    break;

                case "equal":
                    fullfilled = eventValue.compareTo(value) == 0;
                    break;

                case "notequal":
                    fullfilled = eventValue.compareTo(value) != 0;
                    break;

                case "lte":
                    fullfilled = Float.valueOf(eventValue) <= Float.valueOf(value);
                    break;

                case "less":
                    fullfilled = Float.valueOf(eventValue) < Float.valueOf(value);
                    break;

                default:
                    System.out.println("The condition <" + condition + "> doesn't exist");
            }
        }

        return fullfilled;
    }

    @Transactional(readOnly = false)
    private UserEntity getOrCreateTheUser(Long userId) {

        UserEntity user = null;

        try {
            // The user exists
            user = UserRepository.findById(userId).get();

        } catch(Exception e) {

            try {
                // The user has to be created
                user = new UserEntity();
                user.setId(userId);
                UserRepository.save(user);

            } catch (Exception e2) {

                // One thread will create the user, so we wait to retrieve him
                boolean isLocked = true;

                while(isLocked) {
                    try {
                        user = UserRepository.findById(userId).get();
                        isLocked = false;

                    } catch (Exception e3) {
                        // still waiting for the creation
                    }
                }
            }
        }

        return user;
    }

    @Transactional(readOnly = false)
    private void updateUserPoints(UserEntity user, RuleEntity ruleFromDb) {

        // Get the actual total of user's points
        UserPointScalesEntity userPoints = UserPointScalesRepository.findByUserAndPointScale(user, ruleFromDb.getPointScale());

        if(userPoints == null) {

            // Create the user's point scale with the points received
            userPoints = new UserPointScalesEntity();
            userPoints.setUser(user);
            userPoints.setPointScale(ruleFromDb.getPointScale());
            userPoints.setPoints(ruleFromDb.getPoints());

        } else {
            // Update the user's points
            userPoints.setPoints(userPoints.getPoints() + ruleFromDb.getPoints());
        }

        UserPointScalesRepository.save(userPoints);
    }
}
