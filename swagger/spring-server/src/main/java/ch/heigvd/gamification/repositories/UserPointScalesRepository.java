package ch.heigvd.gamification.repositories;

import ch.heigvd.gamification.entities.*;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.LockModeType;

/**
 * Repository for UserPointScales.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
public interface UserPointScalesRepository extends CrudRepository<UserPointScalesEntity, Long> {

    Iterable<UserPointScalesEntity> findAllByUser(UserEntity user);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    UserPointScalesEntity findByUserAndPointScale(UserEntity user, PointScaleEntity pointScale);
}