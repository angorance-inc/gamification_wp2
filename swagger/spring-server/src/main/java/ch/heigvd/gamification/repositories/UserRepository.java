package ch.heigvd.gamification.repositories;

import ch.heigvd.gamification.entities.UserEntity;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.LockModeType;
import java.util.Optional;

/**
 * Repository for User.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
public interface UserRepository extends CrudRepository<UserEntity, Long>{

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<UserEntity> findById(Long id);
}

