package ch.heigvd.gamification.repositories;

import ch.heigvd.gamification.entities.*;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for Rule.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
public interface RuleRepository extends CrudRepository<RuleEntity, Long>{

    Iterable<RuleEntity> findAllByApplication(ApplicationEntity application);
    Iterable<RuleEntity> findAllByApplicationAndType(ApplicationEntity application, String type);
}

