package ch.heigvd.gamification.repositories;

import ch.heigvd.gamification.entities.*;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for PointScale.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
public interface PointScaleRepository extends CrudRepository<PointScaleEntity, Long>{

    Iterable<PointScaleEntity> findAllByApplication(ApplicationEntity application);
}
