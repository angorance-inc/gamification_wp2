package ch.heigvd.gamification.api.endpoints;

import ch.heigvd.gamification.api.RulesApi;
import ch.heigvd.gamification.api.model.*;
import ch.heigvd.gamification.entities.*;
import ch.heigvd.gamification.repositories.*;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.*;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-07-26T19:36:34.802Z")

/**
 * Controller for API Rules.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Controller
public class RulesApiController implements RulesApi {

    @Autowired
    RuleRepository RuleRepository;

    @Autowired
    BadgeRepository BadgeRepository;

    @Autowired
    PointScaleRepository PointScaleRepository;

    @Autowired
    ApplicationRepository ApplicationRepository;

    public ResponseEntity<List<Rule>> getRules(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization) {

        List<Rule> rules = new ArrayList<>();

        // Get all rules of the application
        ApplicationEntity application = ApplicationRepository.findByKey(authorization);

        for (RuleEntity ruleFromDb : RuleRepository.findAllByApplication(application)) {
            rules.add(toRule(ruleFromDb));
        }

        return ResponseEntity.ok(rules);
    }

    public ResponseEntity<Void> createRule(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                           @ApiParam(value = "" ,required=true ) @RequestBody Rule rule) {
        // Prepare the new rule
        RuleEntity newRuleEntity = toRuleEntity(rule);

        // Add the application key to the rule
        ApplicationEntity application = ApplicationRepository.findByKey(authorization);
        newRuleEntity.setApplication(application);

        // Create the new rule
        RuleRepository.save(newRuleEntity);

        // Create the location URI of the created rule
        Long id = newRuleEntity.getId();

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(newRuleEntity.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    public ResponseEntity<Void> updateRule(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                           @ApiParam(value = "",required=true ) @PathVariable("ruleId") Long ruleId,
                                           @ApiParam(value = "" ,required=true ) @RequestBody Rule rule) {
        // Get the existing rule
        RuleEntity ruleFromDb = RuleRepository.findById(ruleId).get();

        // Update the rule
        ruleFromDb.setType(rule.getIf().getType());

        ruleFromDb.setProperty(rule.getIf().getProperty().getName());
        ruleFromDb.setCondition(rule.getIf().getProperty().getCondition());
        ruleFromDb.setValue(rule.getIf().getProperty().getValue());

        ruleFromDb.setPoints(rule.getThen().getAwardPoints().getPoints());

        // Save the rule
        RuleRepository.save(ruleFromDb);

        return ResponseEntity.accepted().build();
    }

    public ResponseEntity<Void> deleteRule(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                           @ApiParam(value = "",required=true ) @PathVariable("ruleId") Long ruleId) {
        // Get the existing rule
        RuleEntity ruleFromDb = RuleRepository.findById(ruleId).get();

        // Delete the rule
        RuleRepository.delete(ruleFromDb);

        return ResponseEntity.ok().build();
    }

    private RuleEntity toRuleEntity(Rule rule) {

        RuleEntity ruleFromDb = new RuleEntity();

        // Create the if part
        ruleFromDb.setType(rule.getIf().getType());

        if(rule.getIf().getProperty().getName() != null) {
            ruleFromDb.setProperty(rule.getIf().getProperty().getName());
            ruleFromDb.setCondition(rule.getIf().getProperty().getCondition());
            ruleFromDb.setValue(rule.getIf().getProperty().getValue());
        }

        // Create the then part
        if(rule.getThen().getAwardBadge() != null) {
            BadgeEntity badge = BadgeRepository.findById(Long.valueOf(rule.getThen().getAwardBadge())).get();
            ruleFromDb.setBadge(badge);
        }

        if(rule.getThen().getAwardPoints().getPointScale() != null) {
            PointScaleEntity pointScale = PointScaleRepository.findById(Long.valueOf(rule.getThen().getAwardPoints().getPointScale())).get();
            ruleFromDb.setPointScale(pointScale);
            ruleFromDb.setPoints(rule.getThen().getAwardPoints().getPoints());
        }

        //ruleFromDb.setName(rule.getName());
        return ruleFromDb;
    }

    private Rule toRule(RuleEntity ruleFromDb) {

        // Create the if part
        RuleIfProperty property = new RuleIfProperty();

        if(ruleFromDb.getProperty() != null) {
            property.setName(ruleFromDb.getProperty());
            property.setCondition(ruleFromDb.getCondition());
            property.setValue(ruleFromDb.getValue());
        }

        RuleIf ifPart = new RuleIf();
        ifPart.setType(ruleFromDb.getType());
        ifPart.setProperty(property);

        // Create the then part
        RuleThenAwardPoints awardPoints = new RuleThenAwardPoints();

        if(ruleFromDb.getPointScale() != null) {
            awardPoints.setPointScale(ruleFromDb.getPointScale().getId());
            awardPoints.setPoints(ruleFromDb.getPoints());
        }

        RuleThen thenPart = new RuleThen();
        thenPart.setAwardPoints(awardPoints);

        if(ruleFromDb.getBadge() != null) {
            thenPart.setAwardBadge(ruleFromDb.getBadge().getId());
        }

        // Create the rule
        Rule rule = new Rule();
        rule.setIf(ifPart);
        rule.setThen(thenPart);

        return rule;
    }
}
