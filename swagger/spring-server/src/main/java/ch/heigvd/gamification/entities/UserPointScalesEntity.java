package ch.heigvd.gamification.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity for UserPointScales.
 *
 * Point scales of the user.
 * Example : User xy has a total of 1405 points for the point scale yz
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Entity
public class UserPointScalesEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private UserEntity user;

    @ManyToOne
    private PointScaleEntity pointScale;

    private long points;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public PointScaleEntity getPointScale() {
        return pointScale;
    }

    public void setPointScale(PointScaleEntity pointScale) {
        this.pointScale = pointScale;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }
}

