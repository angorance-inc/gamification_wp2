package ch.heigvd.gamification.entities;

import javax.persistence.*;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Entity for UserBadges.
 *
 * Badges that the user received.
 * Example : User xy received the badge yz yesterday
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Entity
public class UserBadgesEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private UserEntity user;

    @ManyToOne
    private BadgeEntity badge;

    private Timestamp timestamp;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public BadgeEntity getBadge() {
        return badge;
    }

    public void setBadge(BadgeEntity badge) {
        this.badge = badge;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
