package ch.heigvd.gamification.api.endpoints;

import ch.heigvd.gamification.api.PointScalesApi;
import ch.heigvd.gamification.api.model.PointScale;
import ch.heigvd.gamification.entities.*;
import ch.heigvd.gamification.repositories.*;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-07-26T19:36:34.802Z")

/**
 * Controller for API PointScales.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Controller
public class PointScalesApiController implements PointScalesApi {

    @Autowired
    PointScaleRepository PointScaleRepository;

    @Autowired
    ApplicationRepository ApplicationRepository;

    public ResponseEntity<List<PointScale>> getPointScales(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization) {

        List<PointScale> pointScales = new ArrayList<>();

        // Get all PointScales of the application
        ApplicationEntity application = ApplicationRepository.findByKey(authorization);

        for (PointScaleEntity pointScaleFromDb : PointScaleRepository.findAllByApplication(application)) {
            pointScales.add(toPointScale(pointScaleFromDb));
        }

        return ResponseEntity.ok(pointScales);
    }

    public ResponseEntity<Void> createPointScale(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                                 @ApiParam(value = "" ,required=true ) @RequestBody PointScale pointScale) {

        // Prepare the new point scale
        PointScaleEntity newPointScaleEntity = toPointScaleEntity(pointScale);

        // Add the application to the point scale
        ApplicationEntity application = ApplicationRepository.findByKey(authorization);
        newPointScaleEntity.setApplication(application);

        // Create the new point scale
        PointScaleRepository.save(newPointScaleEntity);

        // Create the location URI of the created point scale
        Long id = newPointScaleEntity.getId();

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(newPointScaleEntity.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    public ResponseEntity<Void> updatePointScale(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                                 @ApiParam(value = "",required=true ) @PathVariable("pointScaleId") Long pointScaleId,
                                                 @ApiParam(value = "" ,required=true ) @RequestBody PointScale pointScale) {

        // Get the existing point scale
        PointScaleEntity pointScaleFromDb = PointScaleRepository.findById(pointScaleId).get();

        // Update the point scale
        pointScaleFromDb.setName(pointScale.getName());

        // Save the point scale
        PointScaleRepository.save(pointScaleFromDb);

        return ResponseEntity.accepted().build();
    }

    public ResponseEntity<Void> deletePointScale(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                                 @ApiParam(value = "",required=true ) @PathVariable("pointScaleId") Long pointScaleId) {

        // Get the existing point scale
        PointScaleEntity pointScaleFromDb = PointScaleRepository.findById(pointScaleId).get();

        // Delete the point scale
        PointScaleRepository.delete(pointScaleFromDb);

        return ResponseEntity.ok().build();
    }

    private PointScaleEntity toPointScaleEntity(PointScale pointScale) {
        PointScaleEntity pointScaleFromDb = new PointScaleEntity();
        pointScaleFromDb.setName(pointScale.getName());
        return pointScaleFromDb;
    }

    private PointScale toPointScale(PointScaleEntity pointScaleFromDb) {
        PointScale pointScale = new PointScale();
        pointScale.setName(pointScaleFromDb.getName());
        return pointScale;
    }
}