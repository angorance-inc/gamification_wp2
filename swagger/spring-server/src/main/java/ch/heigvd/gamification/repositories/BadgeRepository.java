package ch.heigvd.gamification.repositories;

import ch.heigvd.gamification.entities.*;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for Badge.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
public interface BadgeRepository extends CrudRepository<BadgeEntity, Long>{

    Iterable<BadgeEntity> findAllByApplication(ApplicationEntity application);
}
