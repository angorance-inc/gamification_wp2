package ch.heigvd.gamification.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity for PointScale.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Entity
public class PointScaleEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @ManyToOne
    private ApplicationEntity application;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApplicationEntity getApplication() {
        return application;
    }

    public void setApplication(ApplicationEntity application) {
        this.application = application;
    }
}
