package ch.heigvd.gamification.repositories;

import ch.heigvd.gamification.entities.*;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for UserBadges.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
public interface UserBadgesRepository extends CrudRepository<UserBadgesEntity, Long> {

    UserBadgesEntity findByBadgeAndUser(BadgeEntity badge, UserEntity user);

    Iterable<UserBadgesEntity> findAllByUser(UserEntity user);

    Iterable<UserBadgesEntity> findAllByBadge(BadgeEntity badge);
}