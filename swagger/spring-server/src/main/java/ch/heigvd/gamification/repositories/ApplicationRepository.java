package ch.heigvd.gamification.repositories;

import ch.heigvd.gamification.entities.ApplicationEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for Badge.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
public interface ApplicationRepository extends CrudRepository<ApplicationEntity, Long>{

    ApplicationEntity findByKey(String key);
}
