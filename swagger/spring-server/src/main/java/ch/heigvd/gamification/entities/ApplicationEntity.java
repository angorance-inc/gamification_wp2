package ch.heigvd.gamification.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity for Application.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Entity
public class ApplicationEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "application_key")
    private String key;

    public long getId() {
        return id;
    }
}

