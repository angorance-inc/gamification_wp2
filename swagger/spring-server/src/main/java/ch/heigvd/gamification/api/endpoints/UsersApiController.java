package ch.heigvd.gamification.api.endpoints;

import ch.heigvd.gamification.api.UsersApi;
import ch.heigvd.gamification.api.model.UserBadge;
import ch.heigvd.gamification.api.model.UserPointScale;
import ch.heigvd.gamification.entities.UserBadgesEntity;
import ch.heigvd.gamification.entities.UserEntity;
import ch.heigvd.gamification.entities.UserPointScalesEntity;
import ch.heigvd.gamification.repositories.UserBadgesRepository;
import ch.heigvd.gamification.repositories.UserPointScalesRepository;
import ch.heigvd.gamification.repositories.UserRepository;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-07-26T19:36:34.802Z")

/**
 * Controller for API Users.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Controller
public class UsersApiController implements UsersApi {

    @Autowired
    UserBadgesRepository UserBadgesRepository;

    @Autowired
    UserPointScalesRepository UserPointScalesRepository;

    @Autowired
    UserRepository UserRepository;

    public ResponseEntity<List<UserBadge>> getUserBadges(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                                         @ApiParam(value = "",required=true ) @PathVariable("userId") Long userId) {
        List<UserBadge> userBadges = new ArrayList<>();

        // Get all badges of the user
        UserEntity user = UserRepository.findById(userId).get();

        for (UserBadgesEntity userBadgesFromDb : UserBadgesRepository.findAllByUser(user)) {
            userBadges.add(toUserBadges(userBadgesFromDb));
        }

        return ResponseEntity.ok(userBadges);
    }

    public ResponseEntity<List<UserPointScale>> getUserPointScales(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                                                   @ApiParam(value = "",required=true ) @PathVariable("userId") Long userId) {
        List<UserPointScale> userPointScales = new ArrayList<>();

        // Get all badges of the user
        UserEntity user = UserRepository.findById(userId).get();

        for (UserPointScalesEntity userPointScalesFromDb : UserPointScalesRepository.findAllByUser(user)) {
            userPointScales.add(toUserPointScales(userPointScalesFromDb));
        }

        return ResponseEntity.ok(userPointScales);
    }

    private UserBadge toUserBadges(UserBadgesEntity userBadgeFromDb) {
        UserBadge userBadge = new UserBadge();

        userBadge.setName(userBadgeFromDb.getBadge().getName());
        userBadge.setDate(userBadgeFromDb.getTimestamp().toString());

        return userBadge;
    }

    private UserPointScale toUserPointScales(UserPointScalesEntity userPointScaleFromDb) {
        UserPointScale userPointScale = new UserPointScale();

        userPointScale.setName(userPointScaleFromDb.getPointScale().getName());
        userPointScale.setPoints(userPointScaleFromDb.getPoints());

        return userPointScale;
    }
}
