package ch.heigvd.gamification.api.endpoints;

import ch.heigvd.gamification.api.BadgesApi;
import ch.heigvd.gamification.api.model.Badge;
import ch.heigvd.gamification.entities.*;
import ch.heigvd.gamification.repositories.*;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.*;
import java.util.*;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-07-26T19:36:34.802Z")

/**
 * Controller for API Badges.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Controller
public class BadgesApiController implements BadgesApi {

    @Autowired
    BadgeRepository BadgeRepository;

    @Autowired
    ApplicationRepository ApplicationRepository;

    public ResponseEntity<List<Badge>> getBadges(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization) {

        List<Badge> badges = new ArrayList<>();

        // Get all badges of the application
        ApplicationEntity application = ApplicationRepository.findByKey(authorization);

        for (BadgeEntity badgeFromDb : BadgeRepository.findAllByApplication(application)) {
            badges.add(toBadge(badgeFromDb));
        }

        return ResponseEntity.ok(badges);
    }

    public ResponseEntity<Void> createBadge(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                @ApiParam(value = "" ,required=true ) @RequestBody Badge badge) {
        
        // Prepare the new badge
        BadgeEntity newBadgeEntity = toBadgeEntity(badge);

        // Add the application key to the badge
        ApplicationEntity application = ApplicationRepository.findByKey(authorization);
        newBadgeEntity.setApplication(application);

        // Create the new badge
        BadgeRepository.save(newBadgeEntity);

        // Create the location URI of the created badge
        Long id = newBadgeEntity.getId();

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(newBadgeEntity.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    public ResponseEntity<Void> updateBadge(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                            @ApiParam(value = "",required=true ) @PathVariable("badgeId") Long badgeId,
                                            @ApiParam(value = "" ,required=true ) @RequestBody Badge badge) {

        // Get the existing badge
        BadgeEntity badgeFromDb = BadgeRepository.findById(badgeId).get();

        // Update the badge
        badgeFromDb.setName(badge.getName());

        // Save the badge
        BadgeRepository.save(badgeFromDb);

        return ResponseEntity.accepted().build();
    }

    public ResponseEntity<Void> deleteBadge(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                     @ApiParam(value = "",required=true ) @PathVariable("badgeId") Long badgeId) {

        // Get the existing badge
        BadgeEntity badgeFromDb = BadgeRepository.findById(badgeId).get();

        // Delete the badge
        BadgeRepository.delete(badgeFromDb);

        return ResponseEntity.ok().build();
    }

    private BadgeEntity toBadgeEntity(Badge badge) {
        BadgeEntity badgeFromDb = new BadgeEntity();
        badgeFromDb.setName(badge.getName());
        return badgeFromDb;
    }

    private Badge toBadge(BadgeEntity badgeFromDb) {
        Badge badge = new Badge();
        badge.setName(badgeFromDb.getName());
        return badge;
    }
}