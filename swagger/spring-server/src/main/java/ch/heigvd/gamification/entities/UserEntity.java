package ch.heigvd.gamification.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity for User.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
@Entity
public class UserEntity implements Serializable {

    @Id
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}


