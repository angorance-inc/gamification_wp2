package ch.heigvd.gamification.repositories;

import ch.heigvd.gamification.entities.*;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for PointsReward.
 *
 * @auhor Héléna Line Reymond, Daniel Gonzalez Lopez, Bryan Curchod
 */
public interface PointsAwardRepository extends CrudRepository<PointsAwardEntity, Long> {

    Iterable<PointsAwardEntity> findAllByUser(UserEntity user);
}
