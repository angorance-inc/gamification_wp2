# AMT_Gamification Project - Part 2

This document describes the second part of our project, its information, tests, bugs and limitations.

## Contributors

* **Bryan Curchod** - [BryCur](https://gitlab.com/BryCur)
* **Daniel Gonzalez Lopez** - [Angorance](https://gitlab.com/Angorance)
* **Héléna Reymond** - [LNAline](https://gitlab.com/LNAline)

## Goal (Instructions)

The goal is to implement a simple domain model (a set of collaborating business classes). It is then to expose this domain model via a REST API. But the goal is not only to comply with functional requirements. As it was the case for WP1, we also want to address non-functional requirements (e.g. scalability, reliability, etc.). We will therefore implement automated testing.

### Simple Domain Model For Gamification

A gamification engine can be very rich and support a lot of game mechanics. Because we have limited time, we will implement a simple model that will be enough to demonstrate the concept of the service. The model has already been presented during the lectures (see slides), but here are key elements:

- Every gamified application will send a stream of events to the gamification platform. Sending an event to the platform is a way to say: "This user has done this action". A gamified GAPS could send an event to say that "Bob has obtained a grade for a course". A gamified Stackoverflow could send an event to say that "Alice has answered a question about Javascript". It is a good idea to include a map of "properties" in the event data structure, so that every application can freely define its own event attributes. At the minimum, an event should have a user id (the user id in the gamified application), a timestamp, an event type and a map of properties. In terms of REST APIs, there should be a /events endpoint, accepting POST requests.
- The developer doing the configuration for a gamified application should be able to configure two game mechanics: badges and point scales. For a gamified application, the developer could decide that he wants a badge named "Newbie" and another one named "Guru". The developer could decide that he wants a point scale named "Experience" and another one named "Creativity". The key point is that badges and point scales are not defined globally. They are defined in the scope of one gamified application. In other words, every application defines its own list of badges and scales. In terms of REST APIs, there should be a /badges and a /pointScales endpoints for the usual CRUD operations (but think about the implications of DELETE and make an informed decision whether to support it or not).
- We also need some sort of rule system to make the link between the stream of events and the award of badges and points to users. Simply stated, the developer must be able to state "If an event with these properties comes in, then give this reward to the user". The rule system is where we can be very creative and more or less flexible. It is actually simple to implement a basic system, for instance by working with the event type property in the event.

## Information About The Project

To launch our dockerized app, please go to the `/topology-amt` folder in the docker terminal and type this command:
```
docker-compose up --build
```

You can then access:
* the [API documentation](http://192.168.99.100:8080/swagger-ui.html), generated from annotations in the code
* the [API endpoint](http://192.168.99.100:8080/), accepting GET, POST, PATCH and DELETE requests

Reachable addresses from the docker app:
- `192.168.99.100:8080`: REST API
- `192.168.99.100:3306`: MySQL
- `192.168.99.100:6060`: PhpMyAdmin

Default server address set in the project:
- `http://localhost:8080`

PhpMyAdmin gamification_api database account:
- username: `gamification_api_user`
- password: `gam_api_us_2018`

README locations:
- `/jmeter-test/README.md`: readme about the JMeter tests and concurency resolution

## Database Schema

<img src="images/mysql/data/gamification_api_schema.png" />  

### Tables description

- `application_entity`: application to gamify
- `user_entity`: user of the application

- `badge_entity`: badge of the application
- `point_scale_entity`: point scale of the application

- `rule_entity`: rule of the application
- `user_badges_entity`: badges won by the user of the application
- `points_award_entity`: points received by a user for a specific point scale after one event
- `user_point_scales_entity`: total number of points won by the user for point scales

### Choices

There is no table for the upcoming events. We don't need it, because all points award and badges won by the user are dated, and events are automatically handled when they arrive.

## Tests

`24 scenarios` have been implemented with Cucumber and all were successfully passed

<img src="cucumber_tests.png" />


### Testing The Gamification Microservice By Running The Executable Specification

You can use the Cucumber project to validate the API implementation. Do this when the server is running.

1. Change the server address in the `pom.xml` of the `gamification-specs` project if `localhost` isn't your docker address

```
<ch.heigvd.gamification.server.url>http://<dockerAddress>:8080</ch.heigvd.gamification.server.url>
```

2. Launch the tests

```
cd swagger/gamification-specs/
mvn clean test
```

You will see the test results in the terminal, but you can also open the file located in `./target/cucumber/index.html`

## Bugs

We didn't find any bugs in our project.

## Limitations
Our project doesn't handle the following feature: `As an application developer, I can define rules that allow me manage some state (e.g. give the badge after 5 events of type "askQuestion") have been asked.`, because we hadn't the time to do it. We had to do some sacrifices this end of semester to have multiple good projects, instead of excellent and bad ones.