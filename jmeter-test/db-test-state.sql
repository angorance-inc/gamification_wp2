-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : mysql
-- Généré le :  jeu. 24 jan. 2019 à 14:17
-- Version du serveur :  5.7.15
-- Version de PHP :  7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gamification_api`
--

-- --------------------------------------------------------

--
-- Structure de la table `application_entity`
--

CREATE TABLE `application_entity` (
  `id` bigint(20) NOT NULL,
  `application_key` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `application_entity`
--

INSERT INTO `application_entity` (`id`, `application_key`) VALUES
(1, 'API_KEY_1'),
(2, 'API_KEY_2'),
(3, 'API_KEY_3'),
(4, 'API_KEY_4'),
(5, 'API_KEY_5'),
(6, 'API_KEY_6'),
(7, 'API_KEY_7'),
(8, 'API_KEY_8'),
(9, 'API_KEY_9');

-- --------------------------------------------------------

--
-- Structure de la table `badge_entity`
--

CREATE TABLE `badge_entity` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `application_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `badge_entity`
--

INSERT INTO `badge_entity` (`id`, `name`, `application_id`) VALUES
(1, 'First buy', 8);

-- --------------------------------------------------------

--
-- Structure de la table `points_award_entity`
--

CREATE TABLE `points_award_entity` (
  `id` bigint(20) NOT NULL,
  `points` bigint(20) NOT NULL,
  `timestamp` datetime(6) DEFAULT NULL,
  `point_scale_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `point_scale_entity`
--

CREATE TABLE `point_scale_entity` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `application_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `point_scale_entity`
--

INSERT INTO `point_scale_entity` (`id`, `name`, `application_id`) VALUES
(1, 'Cresus', 8);

-- --------------------------------------------------------

--
-- Structure de la table `rule_entity`
--

CREATE TABLE `rule_entity` (
  `id` bigint(20) NOT NULL,
  `rule_condition` varchar(255) DEFAULT NULL,
  `points` bigint(20) NOT NULL,
  `property` varchar(255) DEFAULT NULL,
  `rule_type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `application_id` bigint(20) DEFAULT NULL,
  `badge_id` bigint(20) DEFAULT NULL,
  `point_scale_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rule_entity`
--

INSERT INTO `rule_entity` (`id`, `rule_condition`, `points`, `property`, `rule_type`, `value`, `application_id`, `badge_id`, `point_scale_id`) VALUES
(1, NULL, 100, NULL, 'bought something', NULL, 8, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_badges_entity`
--

CREATE TABLE `user_badges_entity` (
  `id` bigint(20) NOT NULL,
  `timestamp` datetime(6) DEFAULT NULL,
  `badge_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_entity`
--

CREATE TABLE `user_entity` (
  `id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_point_scales_entity`
--

CREATE TABLE `user_point_scales_entity` (
  `id` bigint(20) NOT NULL,
  `points` bigint(20) NOT NULL,
  `point_scale_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `application_entity`
--
ALTER TABLE `application_entity`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `badge_entity`
--
ALTER TABLE `badge_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKd9hfwrn2g5ib7vba7pqx8fhki` (`application_id`);

--
-- Index pour la table `points_award_entity`
--
ALTER TABLE `points_award_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKie1omu38idd408h6871qt30t5` (`point_scale_id`),
  ADD KEY `FKmtp0630h8djmrbeojc5k7ra67` (`user_id`);

--
-- Index pour la table `point_scale_entity`
--
ALTER TABLE `point_scale_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK3vwcs4yccavds517bb5gbl5ne` (`application_id`);

--
-- Index pour la table `rule_entity`
--
ALTER TABLE `rule_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcntvm8jcj0usdhethkbhbhvjt` (`application_id`),
  ADD KEY `FKjmkhg6aor8ingdabx4l8ovt6p` (`badge_id`),
  ADD KEY `FKpubbeulrqs01orodq3tx9srgm` (`point_scale_id`);

--
-- Index pour la table `user_badges_entity`
--
ALTER TABLE `user_badges_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKa0ekt0ls836l9mywxpcltfvb9` (`badge_id`),
  ADD KEY `FKt40xj2c4m0mn1pfr8jx8udkr8` (`user_id`);

--
-- Index pour la table `user_entity`
--
ALTER TABLE `user_entity`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_point_scales_entity`
--
ALTER TABLE `user_point_scales_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKn7cl2w2ljdc3ejgagiry2ajuj` (`point_scale_id`),
  ADD KEY `FKqpuy6uk8iqtq0t4l8hfwcw0xd` (`user_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `application_entity`
--
ALTER TABLE `application_entity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `badge_entity`
--
ALTER TABLE `badge_entity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `points_award_entity`
--
ALTER TABLE `points_award_entity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `point_scale_entity`
--
ALTER TABLE `point_scale_entity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `rule_entity`
--
ALTER TABLE `rule_entity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `user_badges_entity`
--
ALTER TABLE `user_badges_entity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user_point_scales_entity`
--
ALTER TABLE `user_point_scales_entity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `badge_entity`
--
ALTER TABLE `badge_entity`
  ADD CONSTRAINT `FKd9hfwrn2g5ib7vba7pqx8fhki` FOREIGN KEY (`application_id`) REFERENCES `application_entity` (`id`);

--
-- Contraintes pour la table `points_award_entity`
--
ALTER TABLE `points_award_entity`
  ADD CONSTRAINT `FKie1omu38idd408h6871qt30t5` FOREIGN KEY (`point_scale_id`) REFERENCES `point_scale_entity` (`id`),
  ADD CONSTRAINT `FKmtp0630h8djmrbeojc5k7ra67` FOREIGN KEY (`user_id`) REFERENCES `user_entity` (`id`);

--
-- Contraintes pour la table `point_scale_entity`
--
ALTER TABLE `point_scale_entity`
  ADD CONSTRAINT `FK3vwcs4yccavds517bb5gbl5ne` FOREIGN KEY (`application_id`) REFERENCES `application_entity` (`id`);

--
-- Contraintes pour la table `rule_entity`
--
ALTER TABLE `rule_entity`
  ADD CONSTRAINT `FKcntvm8jcj0usdhethkbhbhvjt` FOREIGN KEY (`application_id`) REFERENCES `application_entity` (`id`),
  ADD CONSTRAINT `FKjmkhg6aor8ingdabx4l8ovt6p` FOREIGN KEY (`badge_id`) REFERENCES `badge_entity` (`id`),
  ADD CONSTRAINT `FKpubbeulrqs01orodq3tx9srgm` FOREIGN KEY (`point_scale_id`) REFERENCES `point_scale_entity` (`id`);

--
-- Contraintes pour la table `user_badges_entity`
--
ALTER TABLE `user_badges_entity`
  ADD CONSTRAINT `FKa0ekt0ls836l9mywxpcltfvb9` FOREIGN KEY (`badge_id`) REFERENCES `badge_entity` (`id`),
  ADD CONSTRAINT `FKt40xj2c4m0mn1pfr8jx8udkr8` FOREIGN KEY (`user_id`) REFERENCES `user_entity` (`id`);

--
-- Contraintes pour la table `user_point_scales_entity`
--
ALTER TABLE `user_point_scales_entity`
  ADD CONSTRAINT `FKn7cl2w2ljdc3ejgagiry2ajuj` FOREIGN KEY (`point_scale_id`) REFERENCES `point_scale_entity` (`id`),
  ADD CONSTRAINT `FKqpuy6uk8iqtq0t4l8hfwcw0xd` FOREIGN KEY (`user_id`) REFERENCES `user_entity` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
