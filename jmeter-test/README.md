# JMeter Tests

A script is available to set the DB in the testing state. The data inserted via the swagger specification are the following (authorization: `API_KEY_8`): 

- Badge:

```json
{
    "name": "First buy"
}
```
![badge_entity](procedure-screen/04.png)


- Point scale: 

```json
{
    "name": "Cresus"
}
```
![pont_scale_entity](procedure-screen/03.png)


- Rule:

```json
{
    "if": {
        "property": {},
        "type": "bought something"
    },
    "then": {
        "awardBadge": 1,
        "awardPoints": {
            "pointScale": 1,
            "points": 100
        }
    }
}
```
![rule_entity](procedure-screen/07.png)


Event sent with JMeter:

```json
{
  "date": "2019-01-18 17:50:00",
  "properties": {},
  "type": "bought something",
  "userId": 42
}
```

It should create a `user_badges_entity` and a `point_reward_entity`, and should create a `point_scale_entity` if it doesn't exist or else it updates it. The value observed in the tests are the `points` field in the `user_point_scales_entity` entry (total of user's points for a specific point scale).

## First Try

Here are the tests made in the first place. A reset of the database was made between each test: 

| Threads | Queries | Expected | Observed | Result  |
| ------- | ------- | -------- | -------- | ------- |
| 1       | 1       | 100      | 100      | Success |
| 10      | 1       | 1000     | 1000     | Success |
| 100     | 1       | 10000    | 2700     | Fail    |

Observations after some "rerun": 

- The total of points was wrong in `user_point_scales_entity`, but the total of the `points_award_entity` registered was correct. 
- In a run, there was 8 `user_point_scales_entity` simultaneously created, the next 92 requests ended up failing (`javax.persistence.NonUniqueResultException: query did not return a unique result: 8`). 
- In another run, 9 threads failed their request because they wanted to insert a user that already existed (`java.sql.SQLIntegrityConstraintViolationException: Duplicate entry '42' for key 'PRIMARY'`)

## Correction 1

Clearly we had some concurrency problems. We had a big and long discussion about the type of concurrency solution we wanted to choose to solve this problem. 

At first, we thought than an optimistic solution was enough, because we imagined that it wasn't possible for a user to trigger multiple events at the same time (if the app works well). But an argument came out and we changed our minds. Of course if the events are triggered by the user itself, the optimistic solution is sufficient (more reads than writes). But if the user triggers an event for someone else (social network for example), a lot of writes on the same data could happend (more writes than read). In this case, the pessimistic one is a better idea.

As we don't know why the client will use our gamification service, we have to be ready for every case. That's why we choosed to implement the pessimistic solution on the read/write of `user_point_scales_entity`.

`@Lock`
```java
public interface UserPointScalesRepository extends CrudRepository<UserPointScalesEntity, Long> {

    Iterable<UserPointScalesEntity> findAllByUser(UserEntity user);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    UserPointScalesEntity findByUserAndPointScale(UserEntity user, PointScaleEntity pointScale);
}
```

`@Transactional`
```java
@Transactional
    public ResponseEntity<Void> createEvent(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,
                                            @ApiParam(value = "" ,required=true ) @RequestBody Event event) {
    [...]

    // Get the actual total of user's points
    UserPointScalesEntity userPoints = UserPointScalesRepository.findByUserAndPointScale(user,ruleFromDb.getPointScale());

    [...]
}
```


## Second Try

The second test session took place after the implementation of the pessimistic locks. A reset of the database was made between each test

| Threads | Queries | Expected | Observed | Result  |
| ------- | ------- | -------- | -------- | ------- |
| 1       | 1       | 100      | 100      | Success |
| 10      | 1       | 1000     | 1000     | Success |
| 100     | 1       | 10000    | 9100     | Fail    |

Observation:

- 9 threads came back with an error, so 91 threads came to a successful end. Knowing this, there has been 91 `points_award_entity` created, and the total of points of the `user_point_scales_entity`  climbed up to 9100. So *technically* the count is correct. The threads number 1 to 9 where the ones that failed, the number 10 was the first to come back with a successful result. The error given was the following: `o.h.engine.jdbc.spi.SqlExceptionHelper  : Duplicate entry '42' for key 'PRIMARY'` *Reminder, 42 is the userid used in our tests*

Hypothesis: The 10 first threads all sent their request "at the same time", all tried to insert the user but only one succeeded.

## Correction 2

Something was already implemented to verify if the user needed to be created, but apparently that wasn't enough to avoid concurrency problems. So we had to implement the pessimitic solution to the `user_entity` too.

`@Lock`
```java
public interface UserRepository extends CrudRepository<UserEntity, Long>{

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<UserEntity> findById(Long id);
}
```

`@Transaction` was already there
```java
@Transactional
public ResponseEntity<Void> createEvent(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,@ApiParam(value = "" ,required=true ) @RequestBody Event event) {
    
    [...]

    UserEntity user = null;

    try {
        // The user exists
        user = UserRepository.findById(userId).get();

    } catch(Exception e) {
        // The user has to be created
        user = new UserEntity();
        user.setId(userId);
        UserRepository.save(user);
    }

    [...]
}
```


## Third Try

Here are the results of the third test session. A reset of the database was made between each test:

| Threads | Queries | Expected | Observed |      Result       |
| ------: | ------: | -------: | -------: |:---------------: |
|       1 |       1 |      100 |      100 |      Success      |
|      10 |       1 |     1000 |     1000 | ~~fail~~ Success* |
|     100 |       1 |    10000 |     9100 |       Fail        |

\* succeeded at the second try, we had a deadlock at the first and thought that it was "a risk with the pessimistic implementation" and thus retried with 10 threads.

Observations:

- 9 Deadlocks were made in the 100 threads test. We decided that it wasn't an accident anymore. The records were similar to the second try: 91 `points_award_entity` created and 9100 points in the `user_point_scales_entity`. The error thrown was the following: `o.h.engine.jdbc.spi.SqlExceptionHelper  : Deadlock found when trying to get lock; try restarting transaction`.

## Correction 3

If the only errors we had, were the deadlock ones, that was good because we were sure that the pessimistic solution was working. But we tried to handle "manually" the deadlock errors, because we found nothing on the net to help us with that, by forcing the service to retry.

```java
UserEntity user = null;

try {
    // The user exists
    user = UserRepository.findById(userId).get();

} catch(Exception e) {

    try {
        // The user has to be created
        user = new UserEntity();
        user.setId(userId);
        UserRepository.save(user);

    } catch (Exception e2) {

        // One thread will create the user, so we wait to retrieve him
        boolean isLocked = true;

        while(isLocked) {
            try {
                user = UserRepository.findById(userId).get();
                isLocked = false;

            } catch (Exception e3) {
                // still waiting for the creation
            }
        }
    }
}
```

# Fourth Try 

Here are the results of the fourth test session. A reset of the database was made between each test "category" (i.e tens of threads):

| Threads | Queries | Expected | Observed |      Result       |
| ------: | ------: | -------: | -------: |:---------------: |
|       1 |       1 |      100 |      100 |      Success      |
|      10 |       1 |     1000 |     1000 |      Success      |
|     100 |       1 |    10000 |     9100 |       Fail        |
|    1000 |       1 |   100000 |    87000 |       Fail        |
|   10000 |       1 |  1000000 |   820000 |       Fail        |

Observations:

* For experimental purpose, we ran two more tests with 1'000 and 10'000 threads to see if the 9 fails of the 100 threads test was constant. Results: it's not constant but its not linear either. 100 threads result in 9% of failure, 1'000 threads result in 13% failure, and finally 10'000 threads result in 18% failure.
* We also ran each test twice, without cleaning the database in between to see if there was a difference of failure rate. On the second run, we had we had a 100% percent success rate at 1'000 and a 9% failure at 10'000.

## Correction 4

In our implementation we had the two problematic transactions in the same function `createEvent`. So as we hadn't more idea to solve the deadlock problem, we tried to isolate the two functions out of this one to have two separeted transactions instead of one.

```java
@Transactional
public ResponseEntity<Void> createEvent(@ApiParam(value = "" ,required=true ) @RequestHeader(value="authorization", required=true) String authorization,@ApiParam(value = "" ,required=true ) @RequestBody Event event) {
    
    [...]

    // Get the user concerned by the event or create it if not existing
    UserEntity user = getOrCreateTheUser(event.getUserId());

    [...]

    // Add the points to the user's point scale
    updateUserPoints(user, ruleFromDb);
}
```

```java
@Transactional(readOnly = false)
private UserEntity getOrCreateTheUser(Long userId) {

    UserEntity user = null;

    try {
        // The user exists
        user = UserRepository.findById(userId).get();

    } catch(Exception e) {

        try {
            // The user has to be created
            user = new UserEntity();
            user.setId(userId);
            UserRepository.save(user);

        } catch (Exception e2) {

            // One thread will create the user, so we wait to retrieve him
            boolean isLocked = true;

            while(isLocked) {
                try {
                    user = UserRepository.findById(userId).get();
                    isLocked = false;

                } catch (Exception e3) {
                    // still waiting for the creation
                }
            }
        }
    }

    return user;
}
```

```java
@Transactional(readOnly = false)
private void updateUserPoints(UserEntity user, RuleEntity ruleFromDb) {

    // Get the actual total of user's points
    UserPointScalesEntity userPoints = UserPointScalesRepository.findByUserAndPointScale(user, ruleFromDb.getPointScale());

    if(userPoints == null) {

        // Create the user's point scale with the points received
        userPoints = new UserPointScalesEntity();
        userPoints.setUser(user);
        userPoints.setPointScale(ruleFromDb.getPointScale());
        userPoints.setPoints(ruleFromDb.getPoints());

    } else {
        // Update the user's points
        userPoints.setPoints(userPoints.getPoints() + ruleFromDb.getPoints());
    }

    UserPointScalesRepository.save(userPoints);
}
```

## Fifth Try 

Here are the results of the fifth test session. A reset of the database was made between each test:

| Threads | Queries | Expected | Observed | Result  |
| ------: | ------: | -------: | -------: |:-----: |
|       1 |       1 |      100 |      100 | Success |
|      10 |       1 |     1000 |     1000 | Success |
|     100 |       1 |    10000 |     9200 |  Fail   |
|    1000 |       1 |   100000 |    97400 |  Fail   |
|   10000 |       1 |  1000000 |   887900 |  Fail   |

Observations: 

- A light improvement is observed on the 100 threads test, but it's probably luck, the second run showed 10 failed requests.
- The 1'000 threads run did improve its results, with only 2.6% of failure rate (26 failed queries).
- The 10'000 threads test also showed improvements in the result with "only" 11.2% of failure. We think we can say that the solution is now better now than before but still not perfect.

## Conclusion
In conclusion we think that it's hardly possible to implement a working solution with 0 risk of deadlocks. Moreover, in most cases (less than 10'000 threads) the deadlocks were only brought up on the user insertion. Knowing this we think it's even more unlikely that such a case might happen.

## Comments 
During the tests, we noticed that the `API_KEY` was missing in the `user` entity, mistake that we tried to care of by the time but not finished to handle before the presentation (available on branch 42).

